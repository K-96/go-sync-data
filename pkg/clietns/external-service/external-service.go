package externalservice

import (
	"context"
	"errors"
	"sync/atomic"
	"test/internal/models"
	"time"
)

// ErrBlocked reports if service is blocked.
var ErrBlocked = errors.New("blocked")

type Logger interface {
	Infof(msg string, args ...interface{})
}

type Client struct {
	limit   uint64
	waiting time.Duration
	logger  Logger

	isRunning atomic.Bool
	requests  chan models.Batch
	done      chan struct{}
	blocked   atomic.Bool
}

func NewClient(limit uint64, waiting time.Duration, logger Logger) *Client {
	c := &Client{
		limit:    limit,
		waiting:  waiting,
		logger:   logger,
		requests: make(chan models.Batch),
		done:     make(chan struct{}),
	}
	c.run()
	return c
}

func (c *Client) run() {
	c.logger.Infof("client: limit time %s", c.waiting)
	limiter := time.NewTicker(c.waiting)
	go func() {
		c.isRunning.Store(true)
		c.logger.Infof("client: start rate limiting")
		for {
			select {
			case <-c.done:
				c.logger.Infof("client: stop rate limiting")
				limiter.Stop()
				return
			case req := <-c.requests:
				c.blocked.Store(true)
				c.logger.Infof("client: limitation enabled")
				c.logger.Infof("client: request %v", req)
				<-limiter.C
				c.blocked.Store(false)
				c.logger.Infof("client: limitation disable %s")
			}
		}
	}()
}

func (c *Client) GetLimits() (n uint64, p time.Duration) {
	return c.limit, c.waiting
}

// Process имитация реального запроса с rate limiting
func (c *Client) Process(_ context.Context, batch models.Batch) error {
	if c.blocked.Load() {
		// имитация долгой блокировки за нарушение лимитов пропустил
		c.logger.Infof("client: violation of limits")
		return ErrBlocked
	}

	time.Sleep(100 * time.Microsecond) // имитация сетевого взаимодействия

	go func() {
		c.requests <- batch
		c.logger.Infof("client: request added to chanel")
	}()

	return nil
}

func (c *Client) Stop(_ context.Context) {
	if c.isRunning.Load() {
		c.done <- struct{}{}
	}
	close(c.done)
	close(c.requests)
	c.logger.Infof("client: stopped")
}
