package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"test/internal/logger"
	"test/internal/workers"
	externalservice "test/pkg/clietns/external-service"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	log := logger.Logger{}
	durationLimit := 5 * time.Second
	service := externalservice.NewClient(10, durationLimit, log)
	worker := workers.NewSyncDataExternalService(service, time.Second*10, log)
	worker.Run(ctx)

	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	s := <-osSignals

	log.Infof("main: SIGNAL:%s", s)
	log.Infof("cancel ctx, expect all workers to stop")
	cancel()
	time.Sleep(durationLimit + time.Second) // ждем пока все воркеры остановятся
}
