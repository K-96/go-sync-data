package logger

import (
	"fmt"
	"time"
)

type Logger struct {
}

func (l Logger) Infof(msg string, args ...interface{}) {
	l.log("info", msg, args...)
}

func (l Logger) Errorf(msg string, args ...interface{}) {
	l.log("error", msg, args...)
}

func (l Logger) log(level string, msg string, args ...interface{}) {
	prefix := fmt.Sprintf("%s\t[%s]: ", level, time.Now().Format("2006-01-02 15:04:05"))
	fmt.Println(fmt.Sprintf(prefix+msg, args...))
}
