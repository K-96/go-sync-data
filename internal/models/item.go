package models

// Batch is a batch of items.
type Batch []Item

// Item is some abstract item.
type Item struct{}
