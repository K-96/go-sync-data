package workers

import (
	"context"
	"errors"
	"test/internal/models"
	"time"
)

// errBlocked reports if service is blocked.
var errBlocked = errors.New("blocked")

// Service defines external service that can process batches of items.
type Service interface {
	GetLimits() (n uint64, p time.Duration)
	Process(ctx context.Context, batch models.Batch) error
	Stop(ctx context.Context)
}

type Logger interface {
	Infof(msg string, args ...interface{})
	Errorf(msg string, args ...interface{})
}

type SyncDataExternalService struct {
	s               Service
	duration        time.Duration
	durationForStop time.Duration
	limit           uint64
	logger          Logger
}

func NewSyncDataExternalService(s Service, durationForStop time.Duration, logger Logger) *SyncDataExternalService {
	limit, duration := s.GetLimits()
	return &SyncDataExternalService{
		s:               s,
		duration:        duration,
		durationForStop: durationForStop,
		limit:           limit,
		logger:          logger,
	}
}

func (s *SyncDataExternalService) Run(ctx context.Context) {
	d := s.duration + (500 * time.Microsecond)
	ticker := time.NewTicker(d) // добавил 500 микросекунд, чтобы случайно не стрегерить блокировку
	var blockedAt *time.Time

	go func() {
		for {
			select {
			case <-ctx.Done():
				s.logger.Infof("worker: stopped worker SyncDataExternalService by ctx")
				ticker.Stop()
				s.s.Stop(ctx)
				return
			case t := <-ticker.C: // сюда надо добавить метрики и создать алерты, чтобы отслеживать работу воркера
				if blockedAt != nil {
					if blockedAt.Add(s.durationForStop).Before(time.Now()) {
						s.logger.Infof("worker: restoring sending requests")
						blockedAt = nil // если прошло 10 секунд, то считаем что блокировка снята и восстанавливаем отправку запроса
					} else {
						break
					}
				}

				s.logger.Infof("worker: send request")
				err := s.s.Process(ctx, make(models.Batch, s.limit))
				if errors.As(err, &errBlocked) {
					s.logger.Errorf("worker: catch blocked, stopping requests")
					blockedAt = &t // останавливаем отправку запросов
					break
				}
				if err != nil {
					s.logger.Errorf("worker: error send batch, err: %s", err.Error())
				}

				s.logger.Infof("worker: success send batch")
			}
		}
	}()
}
